-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: symfony
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526C8DB60186` (`task_id`),
  CONSTRAINT `FK_9474526C8DB60186` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,1,'Task 100909','2018-11-23 14:11:38',''),(2,1,'Task 100909','2018-11-23 14:12:59',''),(3,1,'Task 100909','2018-11-23 14:13:11',''),(4,1,'Task 100909','2018-11-24 09:00:21',''),(5,1,'Task 100909','2018-11-24 09:02:57',''),(6,1,'Task 100909','2018-11-24 09:04:30',''),(7,1,'Task 100909','2018-11-24 09:08:32',''),(8,1,'Task 100909','2018-11-24 09:11:37',''),(9,1,'Task 100909','2018-11-24 09:15:21',''),(10,1,'Task 100909','2018-11-24 09:15:33',''),(11,1,'best comment in page\r\n','2018-11-24 11:34:37',''),(12,1,'best comment in page\r\n','2018-11-24 11:35:14',''),(13,2,'имя зе бест','2018-11-24 11:40:55',''),(14,4,'уке','2018-11-24 12:48:31','супер'),(15,10,'asas sa adds','2018-11-26 12:23:57','sk@tut.by'),(16,3,'коммент','2018-11-26 12:40:23','sk@tut.by'),(17,1,'надо задать самую длинную строку в этом задании','2018-11-26 13:27:24','sk@tut.by'),(18,26,'Интересно каким образом это можно сделать?','2018-11-26 19:22:50','sk@tut.by'),(19,27,'комментарий ','2018-11-26 20:03:43','sk@tut.by'),(20,9,'люблю эту задачу','2018-11-26 20:43:33','sss@tut.by'),(21,17,'комментарий 1','2018-11-26 20:46:14','sss@tut.by'),(22,17,'комментарий 1','2018-11-26 20:47:29','sss@tut.by'),(23,17,'комментарий 1','2018-11-26 20:48:03','sss@tut.by'),(24,1,'оставлю еще один комменатрий','2018-11-26 21:06:15','sk@tut.by'),(25,26,'не знаю как ее сделать','2018-11-26 21:07:36','sk@tut.by'),(26,1,'хочу написать самый длинный комментарий к данной задаче и думаю посмотреть что получится','2018-11-28 19:37:28','s01@tut.by'),(27,2,'оставлю комментарий','2018-11-29 07:30:28','sk@tut.by');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_user`
--

DROP TABLE IF EXISTS `fos_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user`
--

LOCK TABLES `fos_user` WRITE;
/*!40000 ALTER TABLE `fos_user` DISABLE KEYS */;
INSERT INTO `fos_user` VALUES (1,'skiv86','skiv86','skiv86@tut.by','skiv86@tut.by',1,NULL,'$2y$13$Lg6w0yHTmzqhNe6/11MuDOlj67V5O9o7a7AgAUgJ.HZFqEYSY5ap.','2018-11-22 17:12:36',NULL,NULL,'a:0:{}'),(2,'sk','sk','sk@tut.by','sk@tut.by',1,NULL,'$2y$13$./gDAphGO.FpvCvABJuOt.ZqMgXwtBfovahXTIAlsobeWx88ZxPgm','2018-11-29 07:13:02',NULL,NULL,'a:0:{}'),(3,'user','user','sss@tut.by','sss@tut.by',1,NULL,'$2y$13$5GKZTXSxU7Y9SgTnPPY/G.DhOpzVahFF0br3dyDiwcRTpZN/yMmam','2018-11-26 20:43:06',NULL,NULL,'a:0:{}'),(4,'so1','so1','s01@tut.by','s01@tut.by',1,NULL,'$2y$13$hfvHRTxutfQ36TFvRBIU3eS4HwBsFt51BQ8epeJCamtKljSbOt84W','2018-11-28 19:36:49',NULL,NULL,'a:0:{}');
/*!40000 ALTER TABLE `fos_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20181122102955'),('20181123120744');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executors` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
INSERT INTO `task` VALUES (1,' Задача номер 1','Новое описание задачи','published','    Andy','    Exec@gmail.com!'),(2,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(3,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(4,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(5,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(6,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(7,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(8,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(9,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(10,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(11,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(12,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(13,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(14,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(15,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(16,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(17,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(18,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(19,'Task 1','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(20,'505','Ergonomic and stylish!','published','    Andy','    Exec@gmail.com!'),(21,'супер название','супер описание','published','    Andy','    Exec@gmail.com!'),(22,' обновил название 33','обновил задачу 3 3 ','published','    Andy','    Exec@gmail.com!'),(23,'задача номер 2','крутая задача','published','    Andy','    Exec@gmail.com!'),(24,'задача 1','описание','published','    Andy','    Exec@gmail.com!'),(25,'65756','56756757','published','    Andy','    Exec@gmail.com!'),(26,'Задача максимализации','Нужно рассчитать возможность зажарки гриля в духовке при 230','published','sk@tut.by','    Exec@gmail.com!'),(27,'Интересная задача','назначить главного ','published','sk@tut.by','skiv86@tut.by'),(28,' хочу обновить','','published','sk@tut.by','sk@tut.by'),(29,'Задача максимализации','необыкновенная задача','published','sk@tut.by','skiv86@tut.by'),(30,' 111','1212 ew','published','sk@tut.by','sk@tut.by');
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-29  7:54:51
