<?php
/**
 * Created by IntelliJ IDEA.
 * User: artyom
 * Date: 22.11.2018
 * Time: 19:00
 */

namespace App\Controller;

use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="hello_action")
     */
    public function helloAction()
    {

        return $this->redirectToRoute('task');

        return $this->render('/base.html.twig', array(

        ));
    }
}