<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\Comment;
use App\Entity\User;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TaskController extends AbstractController
{
    /**
     * @Route("/task", name="task")
     */
    public function index()
    {
        $repository = $this->getDoctrine()
            ->getRepository(Task::class);
        $task = $repository->findAll();

        foreach($task as $t ){
            $taskid = $task[0]->getId();

            $author =0;
            $comment2 = $this->getDoctrine()
                ->getRepository(Task::class)
                ->createQueryBuilder('b')
                ->select('b, c')
                ->leftJoin('b.comments', 'c')
                ->setMaxResults(1)
                ->orderBy('LENGTH(c.text)', 'DESC')
                ->getQuery()
                ->getResult();
        }
        if (!$task) {
            throw $this->createNotFoundException(
                'No task found for id '
            );
        }
        return $this->render('task/index.html.twig', [
            'tasks' => $task,  'comment' => $comment2,
        ]);
    }



/**
 * @Route("/task/{id}", name="task_show")
 */
public function showAction($id)
{
    $time = new \DateTime();
    $task = $this->getDoctrine()
        ->getRepository(Task::class)
        ->find($id);
     $comment = $task->getComments();
    if (!$task) {
        throw $this->createNotFoundException(
            'No task found for id '.$id
        );
    }
    return $this->render('task/show.html.twig', [
        'task' => $task,
        'comment'=>$comment,
        ]);
}
    /**
     * @Route("/task/{id}/newcomment", name="new_comment")
     */
    public function createComment(Request $request, $id)
    {
        $task = $this->getDoctrine()
            ->getRepository(Task::class)
            ->find($id);
        $comment = $task->getComments();
         $em = $this->getDoctrine()->getManager();

//Принимаем параметры с формы
        $params = $request->request->all();
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
          $user = $this->getUser()->getEmail();
        if (empty($params)) {
        }else{
            $time = new \DateTime();
            $comment = new Comment();
            $comment->setText($params['description']);
            $comment->setDate($time);

            //задание автором комментария текущего залогиненого пользователя
            $comment->setAuthor($user);
            $comment->setTask($task);
            $em->persist($task);
            $em->persist($comment);
            $em->flush();
            return $this->render('comment/create-success.html.twig', array(
                'params' => $params,
                'comment'=>$comment,
                'task' => $task,
            ));
        }
        return $this->render('comment/create.html.twig', [

            'params' => $params
        ]);
    }


    /**
     * @Route("/task/createform/", name="task_create")
     */
    public function  createTask(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

//Принимаем параметры с формы
        $params = $request->request->all();
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
        $task = new Task();
        if (empty($params)) {
        }else{
            $user = $this->getUser()->getEmail();
            $task = new Task();
            $task->setName($params['name']) ;
            $task->setDescription($params['description']);
            $task->setStatus('published');
            $task->setAuthor($user);
            $task->setExecutors($params['author']);
            $em->persist($task);
            $em->flush();
            return $this->render('task/create-success.html.twig', array(
                'params' => $params,
                'user' => $user,
                'task' =>$task,
            ));
    }
//Отображаем вид с формой и передаем туда полученные параметры.
        return $this->render('task/create.html.twig', array(
            'params' => $params,
            'user' => $user,
            'task' =>$task,
        ));
    }

    /**
     * @Route("/task/edit/{id}")
     */
    public function updateTask(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $task = $em->getRepository(Task::class)->find($id);
        if (!$task) {
            throw $this->createNotFoundException(
                'No task found for id '.$id
            );
        }
        $params = $request->request->all();
        if (empty($params)) {
        }else{
            $task->setName($params['name']) ;
            $task->setDescription($params['description']);
            $em->flush();
            return $this->render('task/update-success.html.twig', array(
                'params' => $params,
                'task' =>$task,
            ));
        }

//Отображаем вид с формой и передаем туда полученные параметры.
        return $this->render('task/update.html.twig', array(
            'params' => $params,
            'task' =>$task,
        ));
    }
}