<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181124120423 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comment ADD author_id INT NOT NULL, DROP author, CHANGE task_id task_id INT NOT NULL');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CF675F31B FOREIGN KEY (author_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9474526CF675F31B ON comment (author_id)');
        $this->addSql('ALTER TABLE task ADD author_id INT NOT NULL, DROP comments, DROP author');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25F675F31B FOREIGN KEY (author_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_527EDB25F675F31B ON task (author_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CF675F31B');
        $this->addSql('DROP INDEX UNIQ_9474526CF675F31B ON comment');
        $this->addSql('ALTER TABLE comment ADD author VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, DROP author_id, CHANGE task_id task_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB25F675F31B');
        $this->addSql('DROP INDEX UNIQ_527EDB25F675F31B ON task');
        $this->addSql('ALTER TABLE task ADD comments LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, ADD author VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, DROP author_id');
    }
}
